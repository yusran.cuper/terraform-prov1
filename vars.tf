variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "AWS_REGION" {
  default = "ap-southeast-1"
}
variable "AMIS" {
  type = map
  default = {
      us-east-1         = "ami-13be557e"
      us-east-2         = "ami-13be557e"
      ap-southeast-1    =  "ami-01581ffba3821cdf3"
  }
}


